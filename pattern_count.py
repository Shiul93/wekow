import numpy as np

# Input: file (csv/excel), rna row, rna col, maxcols, empty col threshold, initial window size, max window size

rna_row = 0
rna_col = 0
rna_list = ["...(())...)", ".((())).(())...)", "((((()..))))).(())...)"]
maxcols = 0
empty_col_thresh = 1
ini_window_size = 2
max_window_size = 10

cols = {}
last_col = 1

for row_index, rna in enumerate(rna_list):
    for wsize in xrange(ini_window_size, max_window_size+1):
        for offset in range(len(rna) - wsize + 1):
            desc = rna[offset:wsize+offset]
            if (not desc in cols):
                last_col += 1
                cols[desc] = last_col
                #create new column and add name
            col_index = cols[desc]
            f[row_index, col_index] += 1

# read columns starting descriptor count
# calculate zero value percentage and delete column if below empty_col_thresh
