# coding=utf-8
import argparse

# noinspection PyRedundantParentheses
def main():
    parser = argparse.ArgumentParser(description="Change Semicolon CSV files to comma CSV files")
    parser.add_argument('input_csv', metavar='F', type=str, nargs=1,
                        help='route of the semicolon csv file to transform')

    args = parser.parse_args()

    out_str = ""
    with open(args.input_csv[0]) as fp:
        for line in fp:
            out_str += line.replace(';', ',')

    o = open('comma_' + args.input_csv[0], 'w+')
    o.write(out_str)
    o.close()

    print "DONE!"


if __name__ == "__main__":
    main()
