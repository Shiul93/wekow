from excel_read import nextColumn
from openpyxl import load_workbook
from openpyxl import Workbook


collimit = 'EWY'

rowlimit = 4068


startcol = 'D'
startrow = 2



input_excel = load_workbook('./out.xlsx')
for row in range(startrow,rowlimit+1):
    print row
    nextcol = startcol
    while (nextcol != collimit):


        if input_excel.active[nextcol + str(row)].value == None:
            input_excel.active[nextcol + str(row)].value = 0
        nextcol = nextColumn(nextcol)

input_excel.save('./out_nozeros.xlsx')
