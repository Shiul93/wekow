# coding=utf-8
import argparse

from excel_read import nextColumn
from openpyxl import load_workbook
from openpyxl import Workbook


# noinspection PyRedundantParentheses
def main():
    parser = argparse.ArgumentParser(description="""Extract and count micro RNA
        descriptors from secondary structures with overlapping and variable sizing window""")
    parser.add_argument('input_sheet', metavar='F', type=str, nargs=1,
                        help='route of the excel sheet containing secondary structures for mRNA')
    parser.add_argument('row', metavar='R', type=int, nargs=1,
                        help='starting row of the secondary structure data in the input sheet')
    parser.add_argument('col', metavar='C', type=str, nargs=1,
                        help='column of the secondary structure data in the input sheet')
    parser.add_argument('-o', '--out', metavar='O', type=str, nargs=1, default='./out.xlsx',
                        help='route for the output excel sheet containing count results')
    parser.add_argument('-n', '--name', metavar='N', type=str, nargs=1,
                        help='output excel sheet name')
    parser.add_argument('-m', '--max', metavar='M', type=int, nargs=1, default=0,
                        help="""maximum number of processed rows, unlimited by default or zero""")
    parser.add_argument('-M', '--max_desc', metavar='M', type=int, nargs=1, default=0,
                        help="""maximum number of different descriptors, unlimited by default or zero""")
    parser.add_argument('-w', '--min_window', metavar='mW', type=int, nargs=1, default=2,
                        help="""minimum descriptor size, 2 by default""")
    parser.add_argument('-W', '--max_window', metavar='MW', type=int, nargs=1, default=10,
                        help="""maximum descriptor size, 10 by default""")

    args = parser.parse_args()
    output_path = args.out
    rna_row = args.row[0]
    rna_col = args.col[0]
    actualCol = rna_col
    cols = {}
    wsize_dict = {}
    last_col = 'C'
    descriptor_count = 0
    maxrows = args.max[0]
    ini_window_size = args.min_window
    max_window_size = args.max_window

    output_y13 = 'F'
    output_y46 = 'G'
    output_ex_lev = 'H'

    repeat_count = 0
    outindex = 2
    last_segment = ''

    #desc_win_count = 1

    for index in range(20):
        wsize_dict[index] = 1

    input_excel = load_workbook(args.input_sheet[0], read_only=True)
    output = Workbook()

    output.active['A1'].value = 'Y1_3'
    output.active['B1'].value = 'Y4_6'
    output.active['C1'].value = 'Ex_Lev'
    for index in range(rna_row, maxrows+rna_row):
        print index
        actualrna = input_excel.active[rna_col + str(index)].value
        #print actualrna
        length = len(actualrna)
        output.active['A' + str(outindex)].value = input_excel.active[output_y13 + str(index)].value
        output.active['B' + str(outindex)].value = input_excel.active[output_y46 + str(index)].value
        output.active['C' + str(outindex)].value = input_excel.active[output_ex_lev + str(index)].value

        for wsize in xrange(ini_window_size, max_window_size + 1):
            #desc_win_count = 1
            for offset in range(length - wsize + 1):
                desc = actualrna[offset:wsize + offset]
                if (not desc in cols):
                    if (descriptor_count < args.max_desc[0] or args.max_desc[0] == 0):
                        #print desc
                        last_col = nextColumn(last_col)
                        descriptor_count += 1
                        output.active[last_col + '1'].value = 'Fr'+str(wsize)+'-'+str(wsize_dict[wsize]).zfill(4)
                        wsize_dict[wsize] = wsize_dict[wsize] + 1
                        cols[desc] = last_col
                        #desc_win_count += 1
                if (desc in cols):
                    col_index = cols[desc]
                    if (output.active[col_index + str(outindex)].value is None):
                        output.active[col_index + str(outindex)].value = 0
                    if (repeat_count < wsize-1 and last_segment == desc):
                        repeat_count += 1
                    else:
                        output.active[col_index + str(outindex)].value += 1
                        repeat_count = 0
                last_segment = desc

        outindex += 1

    print 'Saving to Excel file'
    output.save(output_path)
    print 'Success'


if __name__ == "__main__":
    main()